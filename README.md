Add this to `composer.json`
    
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/dam1r89/jet-metronic.git"
        }
    ]

Install

    composer require skillfactory/jet-metronic

Publish views:

    php artisan vendor:publish --tag=jet-metronic-views

Publish js assets

  php artisan vendor:publish --tag=jet-metronic-js


### Installation

You need to include metronic frontend in your app.js file:

```javascript
require('./metronic.js');

import VueRouter from 'vue-router';

import routes from './routes';

const router = new VueRouter({
    routes
});

new Vue({
    router,
    mixins: [require('spark')]
});

// optional

new Vue({
    el: '#spark-app-right-panel',
    mixins: [require('spark')]
});
```



### Translations

To define available translations add `locales` key in `config/app.php` with array of locales:

    [
        // ...
        'locale' => 'en',
        'locales' => ['sr', 'en', 'es']
        'fallback_locale' => 'es'
        // ...
    ]

Also you can define `fallback_locale`.

Available commands:

To install frontend dependencies run

    php artisan metronic:install

That command will install:

    npm install --save vue-router vue-shortkey vuejs-datepicker mini-toastr tinymce

And setup `webpack.mix.js` file. Your changes will be overwritten.

Generate CRUD blogs controller

	php artisan metronic:make:controller PostsController Post

Generate basic vue form.

	php artisan metronic:make:form Blogs

Append routes

	php artisan metronic:make:routes Blog BlogsController


Use metronic layout

    @extends('metronic::layout')

    @section('content')
    ...
    @endsection


Programatically add variables for frontend spark elements.

    JetMetronic::frontendVariables(['key' => 'value'])


## Frontend Components

We should have here explanation about folder structure and available components.
Why metronic, ui and components, what to put where, how those components work, how to create for example new value for table.

What is Index.vue, Form.js, Model.js

### How to write routes

Routes example:

```javascript
let post = {
    path: '/posts/:id?',
    component: Index,
    title: 'Pages (link title)',
    icon: 'flaticon-interface-9',
    props: (route) => {
        return {
            api: '/api/posts',
            title: 'Pages (This is page title)',
            labels: [
                {
                    key: 'id',
                    label: 'Id',
                },
                {
                    key: 'title',
                    label: 'Title',
                    sort: true
                },
                {
                    key: 'body',
                    label: 'Body',
                    sort: true
                },
            ],
            formType: 'Post',
            id: route.params.id
        };
    }
  }

export default [post];
```

### Notes

You have to modify `home.blade.php` to have `<router-view></router-view>`.

TODO: create main view and route which will have `router-view`

    run npm watch

<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-light " v-cloak>
    <!-- BEGIN: Aside Menu -->
    <div is="navigation" inline-template>

        <div

                id="m_ver_menu"
                class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
                data-menu-vertical="true"
                data-menu-scrollable="false" data-menu-dropdown-timeout="500"
        >
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                <li class="m-menu__item " aria-haspopup="true" >
                    <a  href="../../index.html" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-line-graph"></i>
                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    Dashboard
                                                </span>
                                                <span class="m-menu__link-badge">
                                                    <span class="m-badge m-badge--danger">
                                                        2
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                    </a>
                </li>
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Components
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>


                <li class="m-menu__item  m-menu__item--submenu"
                    v-for="route in routes" :to="route"
                    aria-haspopup="true"
                    :class="{'m-menu__item--open m-menu__item--expanded': (new RegExp('^'+route.path)).exec($route.path) }"
                    data-menu-submenu-toggle="hover">
                    <a  href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon" :class="route.icon"></i>
                        <span class="m-menu__link-text">@{{ route.title }}</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu" v-show="(new RegExp('^'+route.path)).exec($route.path)">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li v-if="!route.links" class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                <router-link href="#" class="m-menu__link" :to="route"><span class="m-menu__link-text">Locations</span></router-link>
                            </li>

                            <li v-if="route.links" class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                <a  href="#" class="m-menu__link "><span class="m-menu__link-text">Locations</span></a>
                            </li>
                            <router-link v-if="route.links" v-for="sub in route.links" :to="sub" class="m-menu__item " tag="li" active-class="m-menu__item--active" aria-haspopup="true">
                                <a  class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                    <span class="m-menu__link-text">@{{ sub.title }}</span>
                                </a>
                            </router-link>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <!-- END: Aside Menu -->
</div>

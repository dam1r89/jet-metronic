require('./ui');
require('./metronic/index');

Vue.prototype.$user = Spark.state.user;

import Translations from './lang';

Vue.use(Translations);

// Configure mini toastr

import toastr from 'mini-toastr';
toastr.init();


Vue.use(require('vue-shortkey'));

Vue.component('navigation', {
  computed: {
    routes: function() {
      return this.$router.options.routes
        .filter(route => route.hasOwnProperty('title'))
        .map(route => {
          if (route.children){
            route.links = route.children.filter(route => route.hasOwnProperty('title'));
          }
          return route;
        });
    }
  }
});

const config = Object.assign({
    locales: [],
    current: null,
    default: null,
    active: []
}, Spark.translations);

let $lang = {
    all: config.locales,
    _current: config.current,
    get current() {
        return this._current;
    },
    set current(lang) {
        if (!this.all.includes(lang)) {
            return;
        }
        if (this.active.length === 1 && this.active[0] === this._current) {
            this.active = [lang];
        }
        this._current = lang;
    },
    default: Spark.translations.locale,
    active: [Spark.translations.current],
    isActive,
    isCurrent,
    toggle,
};

function toggle(lang) {
    if (!this.all.includes(lang)) {
        return;
    }
    if (this.active.includes(lang)) {
        this.active = this.active.filter(x => x !== lang);
        if (this.active.length === 0) {
            this.active.push(this.current);
        }
    }
    else {
        this.active.push(lang);
    }
}

function isActive(lang) {
    return this.active.includes(lang);
}

function isCurrent(lang) {
    return lang == this._current;
}

export default {
    install(Vue) {

        Vue.mixin({
            data() {
                return { $lang: $lang };
            }
        });
        
        Vue.prototype.$lang = $lang;
    }
};
Vue.component('Block', require('./Block.vue'));
Vue.component('Index', require('./Index.vue'));
Vue.component('ActivityLog', require('./ActivityLog.vue'));
Vue.component('Subheader', require('./Subheader.vue'));

Vue.component('FormInput', require('./form/Input.vue'));
Vue.component('FormInputTrans', require('./form/InputTrans.vue'));
Vue.component('FormTextareaTrans', require('./form/TextareaTrans.vue'));
Vue.component('ColumnRelation', require('./table/header/ColumnRelation.vue'));
Vue.component('ColumnSelect', require('./table/header/ColumnSelect.vue'));
Vue.component('ColumnBoolean', require('./table/header/ColumnBoolean.vue'));
Vue.component('DateRange', require('./table/header/DateRange.vue'));
Vue.component('ColumnSearch', require('./table/header/ColumnSearch.vue'));

Vue.component('CreatedBy', require('./table/value/CreatedBy.vue'));
Vue.component('BooleanYesNo', require('./table/value/BooleanYesNo.vue'));
Vue.component('BooleanTransColorDot', require('./table/value/BooleanTransColorDot.vue'));
Vue.component('Date', require('./table/value/Date.vue'));
Vue.component('Translatable', require('./table/value/Translatable.vue'));
Vue.component('TableImage', require('./table/value/TableImage.vue'));

Vue.component('LinkToPage', require('./table/value/links/Page.vue'));
Vue.component('LinkToBlog', require('./table/value/links/Blog.vue'));

Vue.component('SlugInput', require('./form/SlugInput.vue'));

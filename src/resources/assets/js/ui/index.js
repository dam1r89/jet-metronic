Vue.component('Pagination', require('./Pagination.vue'));
Vue.component('SortLink', require('./SortLink.vue'));
Vue.component('ConfirmPopover', require('./ConfirmPopover.vue'));

Vue.component('Datepicker', require('vuejs-datepicker'));
Vue.component('Timepicker', require('./TimePicker.vue'));
Vue.component('PhotoUpload', require('./PhotoUpload.vue'));
Vue.component('GalleryUpload', require('./GalleryUpload.vue'));
Vue.component('LocationFinder', require('./LocationFinder.vue'));
Vue.component('JsonViewer', require('./JsonViewer.vue'));
Vue.component('Select3', require('./Select3.vue'));
Vue.component('TextEditor', require('./TextEditor.vue'));

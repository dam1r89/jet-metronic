import axios from 'axios';
import FormErrors from './FormErrors.js';

export default function (dataProvider) {

    var form = this;

    const data = dataProvider();
    var keys = Object.keys(data);

    Object.assign(this, data);

    this.errors = new FormErrors();

    this.busy = false;
    this.successful = false;

    this.startProcessing = () => {
        form.errors.forget();
        form.busy = true;
        form.successful = false;
    };

    /**
     * Finish processing the form.
     */
    this.finishProcessing = () => {
        form.busy = false;
        form.successful = true;
    };

    /**
     * Reset the errors and other state for the form.
     */
    this.resetStatus = () => {
        form.errors.forget();
        form.busy = false;
        form.successful = false;
    };


    /**
     * Set the errors on the form.
     */
    this.setErrors = (errors) => {
        form.busy = false;
        form.errors.set(errors);
    };

    this.clear = () => {
        form.resetStatus();
        keys.forEach(prop => {
            form[prop] = undefined;
        });
        // Assign same data like when constructor was made.
        Object.assign(form, dataProvider());
    };

    this.fill = (data) => {
        // Adding keys so latter can remove
        this.errors.forget();
        if (!data) {
            this.clear();
            return;
        }
        Object.keys(data).forEach(key => {
            if (!keys.includes(key)){
                keys.push(key);
            }
        });

        Object.assign(form, JSON.parse(JSON.stringify(data)));
    };

    ['post', 'put', 'patch', 'delete'].forEach(method => {
        this[method] = uri => {
            return this.sendForm(method, uri);
        };
    });


    this.sendForm = (method, uri) => {
        return new Promise((resolve, reject) => {
            form.startProcessing();

            let formData = {};
            keys.forEach(prop => {
                let value = form[prop];
                if (value instanceof Date) {
                    value = formatDate(value);
                }
                formData[prop] = value;

            });

            axios[method](uri, JSON.parse(JSON.stringify(formData)))
                .then(response => {
                    form.finishProcessing();

                    resolve(response.data);
                })
                .catch(errors => {
                    form.setErrors(errors.response.data);

                    reject(errors.response.data);
                });
        });

    };

    function formatDate(date) {
        return date.getUTCFullYear() + '-' +
        ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + date.getUTCDate()).slice(-2) + ' ' +
        ('00' + date.getUTCHours()).slice(-2) + ':' +
        ('00' + date.getUTCMinutes()).slice(-2) + ':' +
        ('00' + date.getUTCSeconds()).slice(-2);
    }

}

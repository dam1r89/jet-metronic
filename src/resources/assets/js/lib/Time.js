import moment from 'moment';
export default {
    methods: {
        toLocal(utc, format = 'D MMM YYYY H:mm') {
            return moment.utc(utc).local().format(format);
        }
    },
};

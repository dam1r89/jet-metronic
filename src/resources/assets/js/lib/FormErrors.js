export default function () {

    this.errors = {};

    this.hasErrors = function () {
        return Object.keys(this.errors).length > 0;
    };


    /**
     * Determine if the collection has errors for a given field.
     */
    this.has = function (field) {
        return this.errors[field] !== undefined;
    };


    /**
     * Get all of the raw errors for the collection.
     */
    this.all = function () {
        return this.errors;
    };


    /**
     * Get all of the errors for the collection in a flat array.
     */
    this.flatten = function () {
        return _.flatten(_.toArray(this.errors));
    };


    /**
     * Get the first error message for a given field.
     */
    this.get = function (field) {
        if (this.has(field)) {
            return this.errors[field][0];
        }
    };


    /**
     * Set the raw errors for the collection.
     */
    this.set = function (errors) {
        if (errors && errors.errors && typeof errors.errors === 'object') {
            this.errors = errors.errors;
        } else {
            this.errors = {'form': ['Something went wrong. Please try again or contact customer support.']};
        }
    };


    /**
     * Remove errors from the collection.
     */
    this.forget = function (field) {
        if (typeof field === 'undefined') {
            this.errors = {};
        } else {
            delete this.errors[field];
        }
    }   ;
}

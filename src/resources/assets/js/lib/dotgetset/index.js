module.exports = {
    getValue: function(key, value, defaultVal = undefined) {

        let elements = key.split('.');
        for (var i = 0; i < elements.length; i++) {
            if (!value) {
                return defaultVal;
            }
            value = value[elements[i]];
        }

        return value;
    },
    setValue: function(key, target, newValue) {
        let value = target;
        let elements = key.split('.');
        for (var i = 0; i < elements.length - 1; i++) {
            if (!value[elements[i]]) {
                value[elements[i]] = {};
            }
            value = value[elements[i]];
        }
        value[elements[elements.length - 1]] = newValue;
    }

};
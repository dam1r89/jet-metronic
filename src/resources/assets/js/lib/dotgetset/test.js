/* eslint-env node, mocha */

let handler = require('./index'),
    assert = require('assert');

describe('test', function() {

    var test = null;
    beforeEach(function() {
        test = {
            name: 'Damir',
            image: {
                url: 'http://',
                alt: {},
            }
        };
    });

    it('should get value simple value', () => {
        assert(handler.getValue('name', test) === 'Damir');
    });

    it('should get value with dot notation', () => {
        assert(handler.getValue('image.url', test) === 'http://');
    });

    it('should return undefined for missing values', () => {
        assert(handler.getValue('something.random', test) === undefined);
    });

    it('should return default value for missing values', () => {
        assert.equal(handler.getValue('something.random', test, 'tralala'), 'tralala');
    });

    it('should return default if object is missing', () => {
        assert.equal(handler.getValue('something.random', null, 'haha'), 'haha');
    });

    it('should set value on existing object', () => {
        handler.setValue('image.alt', test, 'vrednos');
        assert(handler.getValue('image.alt', test) === 'vrednos');
    });

    it('should set value on existing object', () => {
        handler.setValue('image.alt', test, 'vrednos');
        assert(handler.getValue('image.alt', test) === 'vrednos');
    });

    it('should set value on nested non existing object', () => {
        handler.setValue('image.text.en', test, 'hello');
        assert(handler.getValue('image.text.en', test) === 'hello');
    });

    it('should set value on root nested non existing object', () => {
        handler.setValue('title.rs', test, 'Zdravo');
        assert(handler.getValue('title.rs', test) === 'Zdravo');
    });

});
/**
 * Requires end component do define
 * `form` and `method` data property.
 * use `v-model` to bind model.
 */
export default {
    props: {
        value: {
            type: Object,
            default () {
                return {};
            }
        }
    },
    data() {
        return {
            form: {
                id: undefined
            }
        };
    },
    methods: {
        save() {
            if (this.form.busy) {
                return;
            }

            if (!this.method) {
                throw Error('You must define a `method` property/computed property on the model in order to use Model.js save function.');
            }
            const existing = !!this.form.id;
            const method = existing ? `${this.method}/${this.form.id}` : this.method;
            this.form.put(method)
                .then(res => {
                    this.form.fill(res);
                    this.$emit('save', res);
                    if (existing) {
                        this.$emit('update', res);
                    }
                    else{
                        this.$emit('create', res);
                    }
                    // Because loop is created when event handler
                    // like "save" clears the form, it is automatically
                    // populated because of the loop back and watching
                    // for value changes.
                    this.$emit('input', res);
                }, err => {
                    this.$emit('error', err);
                });
        },
        remove() {
            if (this.form.busy) {
                return;
            }
            this.form.delete(`${this.method}/${this.form.id}`)
                .then(() => {
                    this.$emit('remove', this.form);
                    this.clear();
                });
        },
        load() {
            this.form.fill(this.value);
        },
        clear() {
            this.form.clear();
        }
    },
    computed: {
        exists() {
            return this.form.id !== undefined;
        }
    },
    watch: {
        value() {
            this.load();
        }
    },
    mounted() {
        this.load();
    }
};

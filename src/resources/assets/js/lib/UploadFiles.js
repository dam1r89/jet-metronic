import axios from 'axios';

export default {
    data() {
        return {
            files: {}
        };
    },
    methods: {
        upload(id) {
            let keys = Object.keys(this.files).filter(key => this.files[key] != null);
            if (keys.length == 0) {
                return;
            }
            let form = new FormData();

            keys.forEach(key => {
                // TODO: Handle FileList
                if (Array.isArray(this.files[key])) {
                    this.files[key].forEach(file => {
                        form.append(key + '[]', file);
                    });
                } else {
                    form.append(key, this.files[key]);
                }
            });

            axios.post(`${this.method}/${id}/files`, form).then(res => {
                this.files = {};

                let model = res.data;
                this.form.fill(model);
                this.$emit('save', model);
                this.$emit('update', model);
                this.$emit('input', model);
            }, () => {

            });
        }
    },
    mounted() {
        this.$on('save', model => this.upload(model.id));
    }
};
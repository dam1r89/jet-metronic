export default {
    data() {
        return {
            clickHandler: null,
            preventClosing: false,
            visibility: false
        };
    },
    methods: {
        handleOutsideClick() {
            if (this.preventClosing) {
                this.preventClosing = false;
            } else {
                this.visibility = false;
                if (typeof this.outsideClick == 'function') {
                    this.outsideClick();
                }
            }
        },
        preventClose() {
            this.preventClosing = true;
        }
    },
    computed: {
        isVisible() {
            return this.visibility;
        }
    },
    mounted() {
        this.clickHandler = this.handleOutsideClick.bind(this);
        document.addEventListener('click', this.clickHandler, false);
    },
    destroyed() {
        document.removeEventListener('click', this.clickHandler);
    },
};
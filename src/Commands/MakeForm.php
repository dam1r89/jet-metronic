<?php

namespace SkillFactory\JetMetronic\Commands;

use Illuminate\Console\Command;

class MakeForm extends Command
{
    protected $signature = 'metronic:make:form {model}';

    protected $description = 'Generate metronic frontend form.';

    public function handle()
    {
        $data = file_get_contents(__DIR__.'/stubs/Form.vue');
        $model = $this->argument('model');
        file_put_contents(base_path("resources/assets/js/forms/$model.vue"), $data);
        $this->info("Generated metronic frontend form $model");
    }
}

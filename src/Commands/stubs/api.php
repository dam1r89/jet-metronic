
Route::get('__COLLECTION__', '__CONTROLLER__@index');
Route::get('__COLLECTION__/{__ITEM__}', '__CONTROLLER__@get');
Route::put('__COLLECTION__/{__ITEM__?}', '__CONTROLLER__@save');
Route::delete('__COLLECTION__/{__ITEM__}', '__CONTROLLER__@delete');
// Route::post('__COLLECTION__/{__ITEM__?}/files', '__CONTROLLER__@files');
// Route::get('__COLLECTION__/{__ITEM__}/activity', '__CONTROLLER__@activity');

<?php

namespace App\Http\Controllers;

use App\__MODEL__;
use Illuminate\Http\Request;
use App\Images\ImageStore;

class __NAME__ extends Controller
{
    public function index(__MODEL__ $__ITEM__, Request $request)
    {
        // $this->authorize('read', $__ITEM__);

        $query = $__ITEM__->query()
            ->sortBy($request->input())
            ->searchBy($request->input())
            ->matchBy($request->input())
            ->inRange($request->input());

        return $query->latest()->paginate(20);
    }

    public function get(__MODEL__ $__ITEM__)
    {
        return $__ITEM__;
    }

    public function save(__MODEL__ $__ITEM__, Request $request)
    {
        // $this->authorize('save', $__ITEM__);

        $data = $request->validate([
        ]);
        $__ITEM__->fill($data)
            ->save();

        return $__ITEM__;
    }

    public function delete(__MODEL__ $__ITEM__, ImageStore $imageStore)
    {
        // $this->authorize('delete', $__ITEM__);

        $__ITEM__->delete();
        // $imageStore->deleteImages($__ITEM__);

        return 'ok';
    }
}

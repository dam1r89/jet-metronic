let mix = require('laravel-mix');
var path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.less('resources/assets/less/app.less', 'public/css')
   // .less('resources/assets/less/frontend/main.less', 'public/css/frontend.css')
   .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
   .copy('node_modules/sweetalert/dist/sweetalert.css', 'public/css/sweetalert.css')
   .copy('node_modules/tinymce/skins/lightgray/fonts', 'public/css/fonts')
   .copy('node_modules/tinymce/skins/lightgray/skin.min.css', 'public/css/tinymce.css')
   .js('resources/assets/js/app.js', 'public/js')
   // .js('resources/assets/js/frontend/app.js', 'public/js/frontend.js')
   .sourceMaps()
   .extract(['vue', 'jquery', 'bootstrap-sass', 'vuejs-datepicker', 'moment', 'mini-toastr', 'lodash', 'vue-router', 'vue-shortkey', 'axios', 'tinymce'])
   .autoload({
       jquery: ['$', 'jQuery', 'jquery'],
   })
   .webpackConfig({

        resolve: {
            modules: [
                path.resolve(__dirname, 'vendor/laravel/spark/resources/assets/js'),
                'node_modules'
            ],
            alias: {
                'vue$': 'vue/dist/vue.js',
                'lib': path.resolve(__dirname, 'resources/assets/js/lib')
            }
        }
   });

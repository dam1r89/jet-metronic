<?php

namespace SkillFactory\JetMetronic\Commands;

use Illuminate\Console\Command;

class AppendRoutes extends Command
{
    protected $signature = 'metronic:make:routes {model} {controller} {collection?}';

    protected $description = 'Generate routes controller.';

    public function handle()
    {
        $data = file_get_contents(__DIR__.'/stubs/api.php');
        $model = $this->argument('model');
        $data = preg_replace('/__MODEL__/', $model, $data);
        $data = preg_replace('/__ITEM__/', lcfirst($model), $data);

        $controller = $this->argument('controller');
        $data = preg_replace('/__CONTROLLER__/', $controller, $data);

        $collection = $this->argument('collection') ?: snake_case(str_plural($model));
        $data = preg_replace('/__COLLECTION__/', $collection, $data);

        file_put_contents(base_path('routes/api.php'), $data, FILE_APPEND);

        $this->info("Generating routes for $model");
    }
}

<?php

namespace SkillFactory\JetMetronic\Commands;

use Illuminate\Console\Command;

class PublishTheme extends Command
{
    protected $signature = 'metronic:publish';

    protected $description = 'Download Metronic v5 theme in public folder.';

    public function handle()
    {
        $this->info('Getting assets from the web for Version 5...');

        $theme = storage_path('metronic-assets.zip');

        // file_put_contents($theme, fopen('https://www.dropbox.com/s/072x5uy63vewjs8/metronic-theme-v5.zip?dl=1', 'r'));
        file_put_contents($theme, fopen('https://www.dropbox.com/s/qwycw1mpadhdon9/metronic-theme-v5-with-panel.zip?dl=1', 'r'));
        $zip = new \ZipArchive();

        $res = $zip->open($theme);
        if (true !== $res) {
            $this->error('Error opening zip');

            return;
        }
        $this->info('Extracting...');
        $zip->extractTo(public_path());

        file_put_contents(public_path('assets/.gitignore'), '*');

        $zip->close();
        $this->info('Removing files');
        unlink($theme);
    }
}

<?php

namespace SkillFactory\JetMetronic\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    protected $signature = 'metronic:install';

    protected $description = 'Install frontend dependencies.';

    public function handle()
    {
        $this->info('Installing frontend dependecies.');


        chdir(base_path());
        exec('npm install && npm install --save sweetalert moment vue-router vue-shortkey vuejs-datepicker mini-toastr tinymce', $output, $exitCode);

        if ($exitCode === 0) {
            $this->info('Success');
        } else {
            $this->warning('npm dependencies not installed');
        }

        if ($this->confirm('Overwrite webpack.mix.js?')) {
            copy(__DIR__.'/stubs/webpack.mix.js', 'webpack.mix.js');
        }
        $this->info('Downloading metronic assets.');

        $this->call('metronic:publish');
    }
}

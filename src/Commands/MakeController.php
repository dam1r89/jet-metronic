<?php

namespace SkillFactory\JetMetronic\Commands;

use Illuminate\Console\Command;

class MakeController extends Command
{
    protected $signature = 'metronic:make:controller {name} {model}';

    protected $description = 'Generate metronic controller.';

    public function handle()
    {
        $data = file_get_contents(__DIR__.'/stubs/Controller.php');
        $model = $this->argument('model');
        $data = preg_replace('/__MODEL__/', $model, $data);
        $data = preg_replace('/__ITEM__/', lcfirst($model), $data);
        $name = $this->argument('name');
        $data = preg_replace('/__NAME__/', $name, $data);

        file_put_contents(app_path("Http/Controllers/$name.php"), $data);
        $this->info("Generating controller $name");
    }
}

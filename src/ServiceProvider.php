<?php

namespace SkillFactory\JetMetronic;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use SkillFactory\JetMetronic\Commands\AppendRoutes;
use SkillFactory\JetMetronic\Commands\Install;
use SkillFactory\JetMetronic\Commands\MakeController;
use SkillFactory\JetMetronic\Commands\MakeForm;
use SkillFactory\JetMetronic\Commands\PublishTheme;

class ServiceProvider extends LaravelServiceProvider
{
    public function register()
    {
        $this->commands([
            PublishTheme::class,
            MakeController::class,
            MakeForm::class,
            AppendRoutes::class,
            Install::class
        ]);
    }

    public function boot()
    {
        JetMetronic::frontendVariables([
          'Jet' => [
            'translations' => [
                'locales' => config('app.locales', []),
                'current' => app()->getLocale(),
                'locale' => config('app.locale'),
                'fallback_locale' => config('app.fallback_locale', app()->getLocale()),
            ]
          ]
        ]);
        $this->loadViewsFrom(__DIR__.'/resources/views', 'metronic');

        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/metronic'),
        ], 'jet-metronic-views');

        $this->publishes([
            __DIR__.'/resources/assets/js' => resource_path('assets/js'),
        ], 'jet-metronic-js');

        $this->app->call([$this, 'map']);
    }

    public function map(Router $router)
    {
    }
}

<?php

namespace SkillFactory\JetMetronic;

class JetMetronic
{
    private static $variables = [];

    public static function frontendVariables(array $variables = null)
    {
        if ($variables === null) {
            return static::$variables;
        }
        static::$variables = array_merge(static::$variables, $variables);
    }
}
